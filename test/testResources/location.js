export default{
    "resourceType": "Location",
    "id": "1316595",
    "meta": {
        "versionId": "1",
        "lastUpdated": "2018-03-22T00:25:58.096+00:00"
    },
    "name": "South Wing, second floor",
    "description": "Second floor of the Old South Wing, formerly in use by Psychiatry",
    "identifier": [
        {
            "system": "Individual Tax ID",
            "value": "aaal5"
        }
    ],
    "status": "active",
    "operationalStatus": "O",
    "type": "RH",
    "mode": "instance",
    "name": "aaal5",
    "address": {
        "country": "US"
    },
    "managingOrganization": {
        "reference": "Organization/1316369"
    }
}