/**
 @license
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/**
 * `<fhir-active-status>` adds checkbox to the page. Uses mwc-checkbox and iron-ajax
 * In typical use, just use `<fhir-invoice-status url=""></fhir-invoice-status>`
 * @customElement
 * @polymer
 * @demo https://librehealth.gitlab.io/toolkit/lh-toolkit-webcomponents/demos/fhir-invoice-status.html
 *
 */
import { LitElement, html } from '@polymer/lit-element/lit-element.js';
import '@material/mwc-formfield/mwc-formfield.js';
import '@polymer/iron-ajax/iron-ajax.js';

class FhirInvoiceStatus extends LitElement {
  static get properties() {
    return {
      /**typeField is a selectable option type of allergy. Use this property to show/hide. Default: true */
      typeField: String,
      /**url is used to make AJAX call to FHIR resource. Default: null */
      url: String,
      /**value is used to take the input value of each field*/
      value: Array
    };
  }

  /**default value of properties set in constructor*/
  constructor() {
    super();
    this.typeField = 'true';
    this.value = [];
  }

  /**_didRender() delivers only after _render*/
  _didRender() {
    this.shadowRoot
      .getElementById('ajax')
      .addEventListener('iron-ajax-response', function(e) {
        var invoiceStatus = this.parentNode.host;
        if (e.detail.response.category !== undefined) {
          invoiceStatus.value = e.detail.response.category;
        } else {
          this.parentNode.removeChild(
            this.parentNode.querySelector('#statusDiv')
          );
        }
      });
  }

  _render({ typeField, url, value }) {
    return html`
      <div id="statusDiv">
        ${typeField !== 'false'
          ? html`
              <label>Invoice Status:</label>
              <select
                class="typeField"
                value="${this.value}"
                on-change="${e => (this.value.status = e.target.value)}"
              >
                <option value="draft">Draft</option>
                <option value="issued">Issued</option>
                <option value="balanced">Balanced</option>
                <option value="cancelled">Cancelled</option>
                <option value="entered-in-error">Error</option>
              </select>
            `
          : ''}
      </div>
      <iron-ajax
        id="ajax"
        bubbles
        auto
        handle-as="json"
        url="${url}"
      ></iron-ajax>
    `;
  }
}

window.customElements.define('fhir-invoice-status', FhirInvoiceStatus);
